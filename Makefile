TALK=tropt.ss
RACO=raco

all: preview

compile:
	@${RACO} make -v ${TALK}

pict: compile
	@${RACO} pict ${TALK}

preview: compile
	@${RACO} slideshow --widescreen --right-half-screen ${TALK}

show: compile
	@${RACO} slideshow --widescreen ${TALK}

pdf: compile
	@${RACO} slideshow --widescreen --condense --pdf ${TALK}
